package com.ipme.ipmespringbootartifactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IpmeSpringbootArtifactoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(IpmeSpringbootArtifactoryApplication.class, args);
	}
}
